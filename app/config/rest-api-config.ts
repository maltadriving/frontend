/**
 * Created by ksasim on 26.12.16.
 */
export const RestApiConfig = {
    questions: {
        show: '/api/questions/:id'
    },
    training: {
        questions: {
            show: '/api/questions/:id?include=answers.is_correct'
        },
        answers: {
            store: '/api/training/answers'
        }
    }
};
