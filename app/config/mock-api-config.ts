/**
 * Created by ksasim on 26.12.16.
 */
export const MockApiConfig = {
    questions: {
        show: '/api/v1/questions/:id'
    },
    training: {
        questions: {
          show: '/api/v1/questions/:id?include=answers.is_correct'
        },
        answers: {
            store: '/api/training/answers'
        }
    }
};
