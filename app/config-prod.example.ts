import {RestApiConfig} from "./config/rest-api-config";

export const Config = {
    isProduction: true,
    api: RestApiConfig,
    imports: []
};
