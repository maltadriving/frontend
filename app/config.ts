import {MockApiConfig} from "./config/mock-api-config";
import {RestApiConfig} from "./config/rest-api-config";
import {InMemoryWebApiModule} from "angular-in-memory-web-api";
import {InMemoryDataService} from "./services/in-memory-data.service";

export const Config = {
    isProduction: false,
    // api: MockApiConfig,
    api: RestApiConfig,
    imports: [
        // InMemoryWebApiModule.forRoot(InMemoryDataService, {delay: 1200}),
    ]
};
