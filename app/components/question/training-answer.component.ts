/**
 * Created by ksasim on 22.11.16.
 */
/**
 * Created by ksasim on 20.11.16.
 */
import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Answer} from "../../models/answer";
import {TrainingService} from "../../services/training.service";

@Component({
    moduleId: module.id,
    selector: 'md-training-answer',
    templateUrl: './../../views/training-answer.component.html',
    styleUrls: ['./../../css/answer.component.css']
})

export class TrainingAnswerComponent {
    @Input()
    answer: Answer;
}
