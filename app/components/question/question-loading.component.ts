import {Component} from "@angular/core";
/**
 * Created by konstantin.sasim on 10/02/2017.
 */

@Component({
  moduleId: module.id,
  selector: 'md-question-loading',
  templateUrl: './../../views/question-loading.component.html',
  styleUrls: ['./../../css/question-loading.component.css'],
})

export class QuestionLoadingComponent {

}
