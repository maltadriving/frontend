/**
 * Created by ksasim on 20.11.16.
 */
import {Component, OnInit} from '@angular/core';
import {Question} from "./../../models/question";
import {ActivatedRoute, Router, Params} from "@angular/router";
import {QuestionsService} from "../../services/questions.service";
import { CookieService } from 'angular2-cookie/services/cookies.service';


@Component({
    moduleId: module.id,
    selector: 'md-training-question',
    templateUrl: './../../views/training-question.component.html',
    styleUrls: ['./../../css/question.component.css'],
})

export class TrainingQuestionComponent implements OnInit {
    question: Question = null;

    private requestedId: string;

    constructor(
        private route: ActivatedRoute,
        private questionsService: QuestionsService,
        private router: Router,
        private cookies: CookieService
    ){}

    ngOnInit() {
        this.route.params
            .subscribe(params => {
                if( this.requestedId === params['id'] ){
                    return;
                }

                let loadedQuestion =  params['id'];
                let savedQuestion = this.cookies.get('questionId');

                if( loadedQuestion == 'first' ) {
                    if( typeof savedQuestion != 'undefined' ) {
                        this.router.navigate(['/training/question', savedQuestion]);
                    }
                }

                this.requestedId = loadedQuestion;
                this.question = null;
                this.questionsService.loadTrainingQuestion(this.requestedId);
            });
        this.questionsService.getQuestion()
            .subscribe((question: Question) => {
                if( !question.id ){
                    return;
                }
                this.question = question;
                this.redirectIfFirst();

                let expires: Date = new Date();
                expires.setTime(expires.getTime() + 365 * 24 * 60 * 60 * 1000);
                this.cookies.put('questionId', question.sourceId.toString(), expires);
            });
    }

    private redirectIfFirst() {
        if( this.requestedId === 'first' ){
            this.requestedId = this.question.sourceId;
            this.router.navigate(['/training/question', this.question.sourceId]);
        }
    }
}
