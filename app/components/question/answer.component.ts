/**
 * Created by ksasim on 22.11.16.
 */
/**
 * Created by ksasim on 20.11.16.
 */
import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {Answer} from "../../models/answer";
import {TrainingService} from "../../services/training.service";

@Component({
    moduleId: module.id,
    selector: 'md-answer',
    templateUrl: './../../views/answer.component.html',
    styleUrls: ['./../../css/answer.component.css']
})

export class AnswerComponent {
    @Input()
    answer: Answer;
    @Output() onAnswered = new EventEmitter<boolean>();

    state: string = 'unanswered';

    constructor(
        private trainingService: TrainingService
    ){}
    onSelect(answer: Answer): void {
        this.trainingService.getAnswerHttp(answer.id).subscribe(
            (isCorrect) => {
                if( this.state !== 'unanswered' ){
                    return;
                }

                if( !isCorrect ) {
                    this.state = 'incorrect';
                    return;
                }
                this.state = 'correct';
                this.onAnswered.emit(true);
            }
        );
    }
}
