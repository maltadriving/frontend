/**
 * Created by ksasim on 20.11.16.
 */
import {Component, OnInit} from '@angular/core';
import {Question} from "./../../models/question";
import {ActivatedRoute, Router, Params} from "@angular/router";
import {QuestionsService} from "../../services/questions.service";

@Component({
    moduleId: module.id,
    selector: 'md-question',
    templateUrl: './../../views/question.component.html',
    styleUrls: ['./../../css/question.component.css'],
})

export class QuestionComponent implements OnInit {
    question: Question;

    private requestedId: string;

    constructor(
        private route: ActivatedRoute,
        private questionsService: QuestionsService,
        private router: Router,
    ){}

    ngOnInit() {
        this.route.params
            .subscribe(params => {
                if( this.requestedId === params['id'] ){
                    return;
                }
                this.requestedId = params['id'];
                this.questionsService.loadQuestion(this.requestedId);
            });
        this.questionsService.getQuestion()
            .subscribe((question: Question) => {
                if( !question.id ){
                    return;
                }
                this.question = question;
                this.redirectIfFirst();
            });
    }

    private redirectIfFirst() {
        if( this.requestedId === 'first' ){
            this.requestedId = this.question.sourceId;
            this.router.navigate(['/question', this.question.sourceId]);
        }
    }

    onAnswered(event) {
        this.questionsService.addCorrectAnswer();
    }
}
