import {Component, AfterViewInit, ElementRef} from '@angular/core';
/**
 * Created by ksasim on 18.12.16.
 */

@Component({
    moduleId: module.id,
    selector: 'md-ya-metrika',
    templateUrl: './../../views/ya-metrika.component.html'
})

export class YaMetrikaComponent implements AfterViewInit {
    constructor(private elementRef:ElementRef) { };

    ngAfterViewInit() {
        let s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "js/ya-metrika.js";
        this.elementRef.nativeElement.appendChild(s);
    }
}
