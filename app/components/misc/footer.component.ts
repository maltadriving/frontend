import {Component} from "@angular/core";

@Component({
  moduleId: module.id,
  selector: 'md-footer',
  styleUrls: ['./../../css/footer.component.css'],
  templateUrl: './../../views/footer.component.html'
})

export class FooterComponent {}
