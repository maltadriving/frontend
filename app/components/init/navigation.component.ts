/**
 * Created by ksasim on 23.11.16.
 */

import {Component, OnInit} from '@angular/core';
import {QuestionsService} from '../../services/questions.service';
import {QuestionHeader} from '../../models/question-header';

@Component({
    moduleId: module.id,
    selector: 'md-navigation',
    templateUrl: './../../views/navigation.component.html',
    styleUrls: ['./../../css/navigation.component.css'],
})

export class NavigationComponent implements OnInit {
    allAnswered: boolean;

    header: QuestionHeader;

    route: string;

    constructor(
        private questionsService: QuestionsService,
    ) {
        // this.route = '/training/question';
        this.route = '/question';
    }

    ngOnInit() {
        this.questionsService.getIsAllAnswered()
            .subscribe(
                allAnswered => this.allAnswered = allAnswered
            );
        this.questionsService.getHeader()
            .subscribe(
                (header: QuestionHeader) => this.header = header
            );
    }
}
