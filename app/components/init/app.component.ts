import { Component } from '@angular/core';

import '../../rxjs-operators';

@Component({
    moduleId: module.id,
    selector: 'md-app',
    templateUrl: './../../views/app.component.html',
    styleUrls: ['./../../css/app.component.css'],
})

export class AppComponent {
    title = 'Malta driving licence theory';
}
