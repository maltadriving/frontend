/**
 * Created by ksasim on 21.11.16.
 */
import { Question } from './../models/question';
import {Answer} from "../models/answer";

export const QUESTIONS: Question[] = [
    <Question>{
        id: 1,
        text: '1. Before making a U-turn in the road you should always',
        answers: [
            {id: 1, text: 'select a higher gear than normal'},
            {id: 2, text: 'signal so that other drivers can slow down'},
            {id: 3, text: 'look over your shoulder for final confirmation'},
            {id: 4, text: 'give another signal as well as using your indicators'}
        ]
    },
    <Question>{
        id: 2,
        text: '2. Before making a U-turn in the road you should always',
        answers: [
            {id: 5, text: 'select a higher gear than normal'},
            {id: 6, text: 'signal so that other drivers can slow down'},
            {id: 7, text: 'look over your shoulder for final confirmation'},
            {id: 8, text: 'give another signal as well as using your indicators'}
        ]
    },
    <Question>{
        id: 3,
        text: '3. Before making a U-turn in the road you should always',
        answers: [
            {id:  9, text: 'select a higher gear than normal'},
            {id: 10, text: 'signal so that other drivers can slow down'},
            {id: 11, text: 'look over your shoulder for final confirmation'},
            {id: 12, text: 'give another signal as well as using your indicators'}
        ]
    }
];