/**
 * Created by ksasim on 20.11.16.
 */
import {
    InMemoryDbService,
    HttpMethodInterceptorArgs,
    STATUS,
    createObservableResponse,
    createErrorResponse,
    emitResponse
} from 'angular-in-memory-web-api';
import {QUESTION_RESOURCES} from './mock-question-resources';
import {Response, ResponseOptions} from "@angular/http";
import {Observer} from "rxjs/Observer";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {QuestionResource} from "../models/rest/question-resource";
import {TrainingAnswer} from "../models/training-answer";
import {ANSWERS} from "./mock-answers";

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        let questions = QUESTION_RESOURCES;
        let answers   = ANSWERS;
        return {questions,answers};
    }

    protected get(interceptorArgs: HttpMethodInterceptorArgs) {
        // Returns a "cold" observable that won't be executed until something subscribes.
        return new Observable<Response>((responseObserver: Observer<Response>) => {
            let resOptions: ResponseOptions;

            const {id, query, collection, collectionName, headers, req} = interceptorArgs.requestInfo;

            switch (collectionName) {
                case 'questions':
                    let data = this.getQuestionsResponse(collection, id);
                    resOptions = new ResponseOptions({
                        body: JSON.stringify(data),
                        headers: headers,
                        status: STATUS.OK
                    });
                    break;
                default:
                    resOptions = createErrorResponse(req, STATUS.NOT_FOUND,
                        `'${collectionName}' with id='${id}' not found`);
                    break;
            }
            emitResponse(responseObserver, req, resOptions);

            return () => {};
        });
    }

    protected post(interceptorArgs: HttpMethodInterceptorArgs) {
        // Returns a "cold" observable that won't be executed until something subscribes.
        return new Observable<Response>((responseObserver: Observer<Response>) => {
            let resOptions: ResponseOptions;

            const {id, query, collection, collectionName, headers, req} = interceptorArgs.requestInfo;

            switch (collectionName) {
                case 'answers':
                    let data = this.getAnswersResponse(collection, req.json().id);
                    resOptions = new ResponseOptions({
                        body: JSON.stringify({"data":{"value":data.isCorrect ? "1":"0"}}),
                        headers: headers,
                        status: STATUS.OK
                    });
                    break;
                default:
                    resOptions = createErrorResponse(req, STATUS.NOT_FOUND,
                        `'${collectionName}' with id='${id}' not found`);
                    break;
            }

            emitResponse(responseObserver, req, resOptions);

            return () => {};
        });
    }

    private getQuestionsResponse(collection: any[], id: number | string) {
        let data = this.clone(collection);

        if( id === 'first' ) {
            return data[0];
        }

        return data.find((item: QuestionResource) => {
            return +item.data.sourceId === +id;
        });
    }

    private getAnswersResponse(collection: any[], id: number | string): TrainingAnswer {
        let data = this.clone(collection);

        return data.find((item: TrainingAnswer) => {
            return +item.id === +id;
        });
    }

    private clone(data: any) {
        return JSON.parse(JSON.stringify(data));
    }
}
