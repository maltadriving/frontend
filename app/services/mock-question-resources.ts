import {Topic} from "../models/topic";
import {QuestionResource} from "../models/rest/question-resource";
import {Answer} from "../models/answer";
/**
 * Created by ksasim on 21.11.16.
 */

export const QUESTION_RESOURCES: QuestionResource[] = [
    <QuestionResource>{
            "data":{
                "id":99,
                "text":
                    "Which sign means that there may be people walking along the road?\u00a0",
                "sourceNumber":"6.2",
                "sourceId":"4311",
                "orderNumber":2,
                "image":"",
                "topic":{
                    "data":{
                        "id":14,
                        "title":"Vulnerable road users"
                    }
                },
                "answers":{
                    "data":[
                        {
                            "id":1583,
                            "text":null,
                            "image":"images\/CARS_E-685.png",
                            "is_correct": {
                                "data": {"value": false}
                            }
                        },
                        {
                            "id":1584,
                            "text":null,
                            "image":"images\/CARS_E-686.png",
                            "is_correct": {
                              "data": {"value": false}
                            }
                        },
                        {
                            "id":1585,
                            "text":null,
                            "image":"images\/CARS_E-687.png",
                            "is_correct": {
                              "data": {"value": false}
                            }
                        },
                        {
                            "id":1586,
                            "text":null,
                            "image":"images\/CARS_E-688.png",
                            "is_correct": {
                                "data": {"value": true}
                            }
                        }
                        ]
                },
                "correctCount":{
                    "data":{
                        "value":1
                    }
                }
            },
            "meta":{
                "prev":"4310",
                "next":"4312",
                "count":3,
                "currentNumber":2
            }
    },
    <QuestionResource>{
        "data":{
            "id":98,
            "text":
                "You are driving on a quiet country road. What should you expect to see coming towards you on your side of the road?\u00a0",
            "sourceNumber":"6.1",
            "sourceId":"4310",
            "orderNumber":1,
            "image":"",
            "topic":{
                "data":{
                    "id":5,
                    "title":"Vulnerable road users"
                }
            },
            "answers":{
                "data":[
                    {
                        "id":421,
                        "text":" Horse riders\u00a0",
                        "image":null,
                        "is_correct": {
                            "data": {"value": false}
                        }
                    },
                    {
                        "id":422,
                        "text":" Bicycles\u00a0",
                        "image":null,
                        "is_correct": {
                            "data": {"value": false}
                        }
                    },
                    {
                        "id":423,
                        "text":"Pedestrians",
                        "image":null,
                        "is_correct": {
                            "data": {"value": true}
                        }
                    },
                    {
                        "id":424,
                        "text":" Motorcycles\u00a0",
                        "image":null,
                        "is_correct": {
                            "data": {"value": false}
                        }
                    }
                    ]
            },
            "correctCount":{
                "data":{
                    "value":1
                }
            }
        },
        "meta":{
            "prev":"4312",
            "next":"4311",
            "count":3,
            "currentNumber":1
        }
    },
    <QuestionResource>{
        "data":{
            "id":100,
            "text":"You are turning left into a side road and find pedestrians that are crossing. You must:\u00a0",
            "sourceNumber":"6.4",
            "sourceId":"4312",
            "orderNumber":3,
            "image":"",
            "topic":{
                "data":{
                    "id":5,
                    "title":"Vulnerable road users"
                }
            },
            "answers":{
                "data":[
                    {
                        "id":429,
                        "text":" Switch on your hazard lights\u00a0",
                        "image":null,
                        "is_correct": {
                            "data": {"value": false}
                        }
                    },
                    {
                        "id":430,
                        "text":" Sound your horn",
                        "image":null,
                        "is_correct": {
                            "data": {"value": false}
                        }
                    },
                    {
                        "id":431,
                        "text":" Wave them on",
                        "image":null,
                        "is_correct": {
                            "data": {"value": false}
                        }
                    },
                    {
                        "id":432,
                        "text":"Wait for them to cross",
                        "image":null,
                        "is_correct": {
                            "data": {"value": true}
                        }
                    }
                    ]
            },
            "correctCount":{
                "data":{
                    "value":1
                }
            }
        },
        "meta":{
            "prev":"4311",
            "next":"4310",
            "count":3,
            "currentNumber":3
        }
    },
  <QuestionResource>{
    "data":{
      "id":100,
      "text":"You are turning left into a side road and find pedestrians that are crossing. You must:\u00a0",
      "sourceNumber":"6.4",
      "sourceId":"4312",
      "orderNumber":3,
      "image":"",
      "topic":{
        "data":{
          "id":5,
          "title":"Vulnerable road users"
        }
      },
      "answers":{
        "data":[
          {
            "id":429,
            "text":" Switch on your hazard lights\u00a0",
            "image":null,
            "is_correct": {
              "data": {"value": false}
            }
          },
          {
            "id":430,
            "text":" Sound your horn",
            "image":null,
            "is_correct": {
              "data": {"value": false}
            }
          },
          {
            "id":431,
            "text":" Wave them on",
            "image":null,
            "is_correct": {
              "data": {"value": false}
            }
          },
          {
            "id":432,
            "text":"Wait for them to cross",
            "image":null,
            "is_correct": {
              "data": {"value": true}
            }
          }
        ]
      },
      "correctCount":{
        "data":{
          "value":1
        }
      }
    },
    "meta":{
      "prev":"4311",
      "next":"4310",
      "count":3,
      "currentNumber":3
    }
  }
];
