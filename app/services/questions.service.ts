/**
 * Created by ksasim on 20.11.16.
 */

import { Injectable } from '@angular/core';

import {Http, Response, URLSearchParams} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {QuestionTransformer} from "../models/rest/question-transformer";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {QuestionHeader} from "../models/question-header";
import {Question} from "../models/question";
import {AnswerCounter} from "../models/answer-counter";
import {Config} from "../config";

@Injectable()
export class QuestionsService {
    private isAnswered: BehaviorSubject<boolean>;

    private counter: AnswerCounter;

    private observableHeader: BehaviorSubject<QuestionHeader>;
    private observableQuestion: BehaviorSubject<Question>;

    constructor(private http: Http){
        this.isAnswered = <BehaviorSubject<boolean>>new BehaviorSubject(false);
        this.observableHeader = <BehaviorSubject<QuestionHeader>>new BehaviorSubject({});
        this.observableQuestion = <BehaviorSubject<Question>>new BehaviorSubject({});
    }

    loadQuestion(id: string) {
        let url = Config.api.questions.show;
        this.http.get(url.replace(':id', id))
            .map(this.extractData)
            .catch(this.handleError)
            .subscribe(transformer => {
                this.applyTransformer(transformer);
            });
    }

    loadTrainingQuestion(id: string) {
        let url = Config.api.training.questions.show;
        this.http.get(url.replace(':id', id))
            .map(this.extractData)
            .catch(this.handleError)
            .subscribe(transformer => {
                this.applyTransformer(transformer);
            });
    }

    private applyTransformer(transformer: QuestionTransformer) {
        this.observableHeader.next(transformer.header());
        this.observableQuestion.next(transformer.question());
        this.counter = new AnswerCounter(transformer.question());
        this.isAnswered.next(false);
    }

    getHeader(): Observable<QuestionHeader> {
        return this.observableHeader.asObservable();
    }

    getQuestion(): Observable<Question> {
        return this.observableQuestion.asObservable();
    }

    addCorrectAnswer() {
        this.counter.add();
        this.isAnswered.next(this.counter.isCorrect());
    }

    getIsAllAnswered(): Observable<boolean> {
        return this.isAnswered.asObservable();
    }

    private extractData(res: Response) {
        let body = res.json();
        return new QuestionTransformer(body) || {};

    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}