/**
 * Created by ksasim on 21.11.16.
 */
import { Injectable } from '@angular/core';

import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {Config} from "../config";

@Injectable()
export class TrainingService {
    constructor(private http: Http){
    }
    getAnswerHttp(id: number): Observable<boolean> {
        return this.http.post(Config.api.training.answers.store, {id: id})
            .map(this.extractData)
            .catch(this.handleError);
    }
    private extractData(res: Response) {
        let body = res.json();
        return body.data.value === '1';
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}