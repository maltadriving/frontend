/**
 * Created by ksasim on 21.11.16.
 */
import { Question } from './../models/question';
import {Topic} from "../models/topic";
import {Answer} from "../models/answer";
import {TrainingAnswer} from "../models/training-answer";

export const ANSWERS: TrainingAnswer[] = [
    <TrainingAnswer>{
        id: 1583,
        isCorrect: false
    },
    <TrainingAnswer>{
        id: 1584,
        isCorrect: false
    },
    <TrainingAnswer>{
        id: 1585,
        isCorrect: false
    },
    <TrainingAnswer>{
        id: 1586,
        isCorrect: true
    },
    <TrainingAnswer>{
        id: 421,
        isCorrect: false
    },
    <TrainingAnswer>{
        id: 422,
        isCorrect: false
    },
    <TrainingAnswer>{
        id: 423,
        isCorrect: true
    },
    <TrainingAnswer>{
        id: 424,
        isCorrect: false
    },
    <TrainingAnswer>{
        id: 429,
        isCorrect: false
    },
    <TrainingAnswer>{
        id: 430,
        isCorrect: false
    },
    <TrainingAnswer>{
        id: 431,
        isCorrect: false
    },
    <TrainingAnswer>{
        id: 432,
        isCorrect: true
    },
];