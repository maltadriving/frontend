import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './services/in-memory-data.service';

// application
import { QuestionComponent } from './components/question/question.component';
import { AppComponent } from './components/init/app.component';
import {QuestionsService} from './services/questions.service';
import {TrainingService} from './services/training.service';
import {AnswerComponent} from './components/question/answer.component';
import {NavigationComponent} from './components/init/navigation.component';
import {YaMetrikaComponent} from './components/misc/ya-metrika.component';
import {Config} from './config';
import {TrainingQuestionComponent} from "./components/question/training-question.component";
import {TrainingAnswerComponent} from "./components/question/training-answer.component";
import {QuestionLoadingComponent} from "./components/question/question-loading.component";
import {FooterComponent} from "./components/misc/footer.component";

const appRoutes: Routes = [
    { path: 'question/:id', component: QuestionComponent },
    { path: '', redirectTo: '/question/first', pathMatch: 'full'}
    // { path: 'training/question/:id', component: TrainingQuestionComponent },
    // { path: '', redirectTo: '/training/question/first', pathMatch: 'full'}
   // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes),
        //AppRoutingModule,
    ].concat(Config.imports),
    declarations: [
        AppComponent,
        QuestionComponent,
        AnswerComponent,
        NavigationComponent,
        YaMetrikaComponent,
        TrainingQuestionComponent,
        TrainingAnswerComponent,
        QuestionLoadingComponent,
        FooterComponent
    ],
    providers: [
        QuestionsService,
        TrainingService,
        CookieService
    ],
    bootstrap: [ AppComponent ]
})

export class AppModule { }
