import {QuestionResource} from "./question-resource";
import {Question} from "../question";
import {QuestionMeta} from "./question-meta";
import {QuestionHeader} from "../question-header";
/**
 * Created by ksasim on 23.11.16.
 */

export class QuestionTransformer {
    constructor(private resource: QuestionResource){
    }

    header(): QuestionHeader {
        return {
            count: this.resource.meta.count,
            currentNumber: this.resource.meta.currentNumber,
            next: this.resource.meta.next,
            prev: this.resource.meta.prev,
            topic: this.resource.data.topic.data.title,
        };
    }

    question(): Question {
        let questionData = this.resource.data;

        return {
            id: questionData.id,
            sourceId: questionData.sourceId,
            text: questionData.text,
            image: questionData.image,
            answers: questionData.answers.data,
            topic: questionData.topic.data,
            correctCount: questionData.correctCount.data.value
        }
    }
}