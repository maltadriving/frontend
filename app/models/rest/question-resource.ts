
import {Answer} from "../answer";
import {Topic} from "../topic";
import {QuestionMeta} from "./question-meta";
/**
 * Created by ksasim on 23.11.16.
 */
export class QuestionResource {
    data: {
        id: number;
        text: string;
        image: string;
        sourceId: string;
        sourceNumber: string;
        orderNumber: number;
        answers: {
            data: Answer[]
        }
        topic: {
            data: Topic
        },
        correctCount: {
            data: {
                value: number;
            }
        }
    };

    meta: QuestionMeta
}
