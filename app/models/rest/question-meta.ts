/**
 * Created by ksasim on 18.12.16.
 */

export class QuestionMeta {
    count: number;
    currentNumber: number;
    next: string;
    prev: string;
}