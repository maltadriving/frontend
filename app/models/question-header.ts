/**
 * Created by ksasim on 22.11.16.
 */

export class QuestionHeader {
    currentNumber: number;
    topic: string;
    count: number;
    prev: string;
    next: string;
}
