/**
 * Created by ksasim on 20.11.16.
 */
export class Answer {
    id: number;
    text: string;
    image: string;
    is_correct: {
        data: {
            value: boolean
        }
    } = null
}
