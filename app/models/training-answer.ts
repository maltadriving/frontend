/**
 * Created by ksasim on 21.11.16.
 */
export class TrainingAnswer {
    id: number;
    isCorrect: boolean;
}