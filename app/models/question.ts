/**
 * Created by ksasim on 20.11.16.
 */

import { Topic } from './topic';
import { Answer } from './answer';

export class Question {
    id: number;
    sourceId: string;
    text: string;
    image: string;
    topic: Topic;
    answers: Answer[];
    correctCount: number;
}
