import {Question} from "./question";
/**
 * Created by ksasim on 18.12.16.
 */

export class AnswerCounter {
    private count: number;
    private allCorrect: boolean;

    constructor(private question: Question) {
        this.count = 0;
        this.allCorrect = false;
    }

    add() {
        if(this.allCorrect) {
            return;
        }
        this.count++;

        this.allCorrect = (this.count === this.question.correctCount);
    }

    isCorrect() {
        return this.allCorrect;
    }
}