import {RestApiConfig} from "./config/rest-api-config";

export const Config = {
    isProduction: false,
    api: RestApiConfig,
    imports: []
};
